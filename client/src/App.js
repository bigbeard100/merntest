import React, { Component } from 'react';
import axios from 'axios';
import Button from 'react-bootstrap/Button';
import Table from 'react-bootstrap/Table';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Form from 'react-bootstrap/Form';


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      searchText: '',
      clubNameText: '',
      year: 'All',
      selectAll: false
    };

    this.searchTextChange = this.searchTextChange.bind(this);
    this.clubNameTextChange = this.clubNameTextChange.bind(this);
    this.yearChange = this.yearChange.bind(this);
    this.selectAllChange = this.selectAllChange.bind(this);
    this.selectItemChange = this.selectItemChange.bind(this);
  }

  getFilteredDataFromDb = (name, clubName, year) => {
    var queryString = '?';
    if (name) {
      queryString += 'name=' + name + '&';
    }
    if (clubName) {
      queryString += 'clubname=' + clubName + '&';
    }
    if (year) {
      if (year !== 'All') {
        queryString += 'year=' + year + '&';
      }
    }
    
    fetch('http://localhost:3001/api/getData' + queryString)
      .then((data) => data.json())
      .then((res) => this.setState({ data: res.data })
     );

  };

  searchTextChange(event) {
    this.setState({searchText: event.target.value});
  }

  clubNameTextChange(event)
  {
    this.setState({clubNameText: event.target.value});
  }

  yearChange(event)
  {
    this.setState({year: event.target.value});
  }

  selectAllChange(event)
  {
    this.state.data.forEach(function (item, index) {
      item.selected = event.target.checked;
    });

    this.setState({data: this.state.data});
  }

  selectItemChange(event)
  {
  }



  // here is our UI
  // it is easy to understand their functions when you
  // see them render into our screen
  render() {
    const { data } = this.state;
    return (
      <div>
        <div style={{ padding: '10px' }}>
          <InputGroup className="mb-3">
            <FormControl as="select" value={this.state.year} onChange={this.yearChange} >
              <option>All</option>
              <option>2016</option>
              <option>2017</option>
              <option>2018</option>
            </FormControl>
            <FormControl placeholder="Rider name to search for" aria-label="Rider name to search for"
              aria-describedby="basic-addon2" value={this.state.searchText} onChange={this.searchTextChange} 
            />
            <FormControl placeholder="Club name to search for" aria-label="Club name to search for"
              aria-describedby="basic-addon2" value={this.state.clubNameText} onChange={this.clubNameTextChange} 
            />
            <InputGroup.Append>
              <Button  variant="outline-secondary" 
                onClick={() => this.getFilteredDataFromDb(this.state.searchText, this.state.clubNameText, this.state.year) }>
                  Search
              </Button>
            </InputGroup.Append>
          </InputGroup>
        </div>
        <Table striped bordered hover variant="dark">
          <thead>
            <tr>
              <th>Year</th>
              <th>Position</th>
              <th>Number</th>
              <th>Name</th>
              <th>Award</th>
              <th>Time</th>
              <th><Form.Check type="checkbox" checked={data.selectAll} label="Select all" onChange={this.selectAllChange}/></th>
            </tr>
          </thead>
          <tbody>
            { data.map(result => (
              <tr key={result.year + result.number}>
                <td>{result.year}</td>
                <td>{result.position}</td>
                <td>{result.club}</td>
                <td>{result.name}</td>
                <td>{result.award}</td>
                <td>{result.finish}</td>
                <td><Form.Check type="checkbox" checked={result.selected} onChange={this.selectItemChange}/></td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
    );
  }
}

export default App;