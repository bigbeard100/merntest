const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// this will be our data base's data structure 
const resultsSchema = new Schema({
    year: String,
    position: String,
    number: String,
    name: String,
    club: String,
    category: String,
    timetaken: String,
    award: String,
    generation: String,
    newcomer: String,
    events: String,
    generationnames: String,
    ingleborough: String,
    coldcotes: String,
    Whernside: String,
    ribblehead: String,
    penyghent: String,
    finish: String,
    selected: Boolean
});

// export the new Schema so we could modify it using Node.js
exports.results = mongoose.model("result", resultsSchema, "results");
