const mongoose = require('mongoose');
const express = require('express');
var cors = require('cors');
const bodyParser = require('body-parser');
const logger = require('morgan');
const data = require('./data');
const results = data.results;

const API_PORT = 3001;
const app = express();
app.use(cors());
const router = express.Router();

const dbRoute = 'mongodb://localhost:27017/threepeaks'

// connects our back end code with the database
mongoose.connect(dbRoute, { useNewUrlParser: true });
let db = mongoose.connection;

db.once('open', () => console.log('connected to the database'));

// checks if connection with the database is successful
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// (optional) only made for logging and
// bodyParser, parses the request body to be a readable json format
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger('dev'));

// this is our get method
// this method fetches all available data in our database
router.get('/getData', (req, res) => {
    var fieldToSearch = false;
    var nameToFind = '';
    var clubToFind = '';
    var filter = {};

    if (req.query.name)
    {
        console.log('name', req.query.name)
        fieldToSearch = true;
        filter.name = { '$regex' : req.query.name, '$options' : 'i'}
    }

    if (req.query.clubname)
    {
        console.log('clubname', req.query.clubname)
        fieldToSearch = true;
        filter.club = { '$regex' : req.query.clubname, '$options' : 'i' }
    }

    if (req.query.year)
    {
        console.log('year', req.query.year)
        fieldToSearch = true;
        filter.year = req.query.year;
    }

    if (!fieldToSearch)
    {
        console.log('No filter')
        return {};
    }

    console.log(filter);
    results.find(filter, (err, data) => {
        if (err) return res.json({ success: false, error: err });
        //console.log(data);
        return res.json({ success: true, data: data });
    });
});

// append /api for our http requests
app.use('/api', router);

// launch our backend into a port
app.listen(API_PORT, () => console.log(`LISTENING ON PORT ${API_PORT}`));